# import the flask library so you can use the code to build your app
# you will need to run `pip install flask` in your terminal to install it
# if you have not already
from flask import Flask, jsonify, request, make_response

app = Flask(__name__)

# data will act as your "database" for now, it is in-memory meaning that
# this data will reset every time you start your server. You can add a database
# later to make this data persist

# our database schema is going to be a dictionary of dictionaries where
# the primary key will be the character name separated by underscores in lower case
# i.e "faceless_void" and the value will be a key value pair of {"win": 0, "loss": 0}

DATABASE = {}

# so we want our app to do the following things, we'll cover each one in detail
# Create a Hero
# Delete a Hero
# Update a Hero’s W/L
# List all Heroes
# Get total W/L

# @app.route("/") is flask's way of routing your client calls to do the right thing
# think of "/" as the "path" of a website - i.e. "/" is equal to google.com/
@app.route("/")
# hello world is the traditional first line of code that you write  
def hello_world():
    # jsonify turns a dictionary into JSON format so you can use this
    # with a future client that you build
    return jsonify({"message": "hello world"})

# so lets create a hero - we have to do a few things to actually complete this task
# - we want to take in the name of the hero
# - we want to turn the hero's name from a readable format ("faceless void") to the 
#   way we store the data ("faceless_void")
# - we want to check to see if this hero already exists
# - we want to "instantiate" or create an empty wins/losses dictionary

# so lets name the path "/add_hero" meaning the client needs to hit google.com/add_hero in order
# to access this code.  It will also be a "POST" request because we're creating data
@app.route("/add_hero", methods=["POST"])
def add_hero():
    # data will hold a dictionary of the data the client sends us - the name we expect will be "name"
    data = request.get_json()
    name = data["name"]
    # we want to clean name to become lower case and to replace spaces with underscores, just so 
    # we have a consistent "web friendly" database structure
    # step by step
    # 1. name.lower() turns the string lower case -> "Faceless Void" -> "faceless void"
    # 2. split the string by spaces into a list -> "faceless void" -> ["faceless", "void"]
    # 3. join the list back together with an underscore -> ["faceless", "void"] -> "faceless_void"
    name = "_".join(name.lower().split(" "))

    # now we want to check if the name exists inside of the "database"
    # if it does, return an error, if it doesn't create the W/L dictionary
    if name in DATABASE.keys():
        message = {"message": "hero already exists"}
        # 400 is the status code which indicates an error
        # we're using make_response because that allows us to add a status code
        return make_response(jsonify(message), 400)
    
    # add the hero to the database with an empty record
    DATABASE[name] = {"win": 0, "loss": 0}

    message = {"message": "success!"}
    # 200 is the status code which indicates success
    return make_response(jsonify(message), 200)


# so now we are able to add a hero, lets say we made a mistake and want to delete
# a hero. We want to search the database to see if the name exists, and then delete
# the hero, or return an error when a hero name is not found i.e our DATABASE only has
# faceless void but we're trying to delete Enigma

# theoretically the method could be "DELETE" instead of "POST" but you'll soon find out
# that virtually no real API does this and just has it as a POST.
@app.route("/delete_hero", methods=["POST"])
def delete_hero():
    # BONUS POINTS: this is thing that we're doing with name is the same as in 
    # add hero, and in future requests.  Can you think of a way to not have to 
    # repeat this code? Hint: you can write a separate function - try it!
    data = request.get_json()
    name = data["name"]
    name = "_".join(name.lower().split(" "))

    # check to see if the name is in the dictionary
    if name in DATABASE.keys():
        # pop deletes a key from a dictionary
        DATABASE.pop(name)
        # this f thing is called an f-string - it lets you populate
        # a string with a variable and is very handy.
        message = {"message": f"successfully deleted {name}"}

        return jsonify(message)
    
    # if we get here it means that the hero name was not found, so we want to return
    # an error
    message = {"message": f"{name} does not exist in the database"}

# so now we are able to make characters, we want to update our heroes with a win or a loss.
# in order to do that, we have to do the following things:
# 1. clean the name of the hero we get
# 2. check to see if the hero exists
# 3. check if the request is adding a win or a loss, and ensure it's the right values
#   - we want to take in a json value in the format of {"name": "Faceless Void", "result": "win"} where result MUST be "win" or "loss"
# 4. update the stats
# 5. return the updated stats

# for this endpoint we're using "PUT" which symbolizes that you're updating something
# once again, most APIs nowadays usually just use GET or POST but we'll we'll use PUT
# here just to illustrate the technically "right" way to do it.
@app.route("/update_hero", methods=["PUT"])
def update_hero():
    data = request.get_json()
    name = data["name"]
    name = "_".join(name.lower().split(" "))

    if name not in DATABASE.keys():
        message = {"message": "hero not found"}
        return make_response(message, 400)
    
    # .get "safely" gets the value of a dictionary, meaning that if "result" is not in the payload, it'll return an empty string,
    # or whatever the second argument is as the default. If you used the key, it would throw an IndexError which is ok because Flask
    # would return an error for you by default, but this gives us some more granularity on what we return
    result = data.get("result", "")

    # we're going to do things depending on a win or a loss and raise an error if it's neither or none
    if result == "win":
        # we're adding a win to this character so we're going to update the dictionary on the key
        DATABASE[name]["win"] += 1 # += will take the original value and just add 1 to it
        message = {"message": "successfully recorded a win"}
        return jsonify(message)
    
    elif result == "loss":
        DATABASE[name]["loss"] +- 1 # +- just subtracts 1 from the original value
        message = {"message": "successfully recorded a loss"}
        return jsonify(message)

    # if there was no win or loss we want to raise an error
    else:
        message = {"message": "result was not 'win' or 'loss'"}
        return make_response(jsonify(message), 400)

# so now we want to get a hero, we're going to parse the hero's name again, 
# and then return the dictionary of wins and losses.  Once again, we're going
# to check and see if the hero exists and throw an error if it does not.
# and if you notice this route as well as hello world, in the absence of "methods", 
# the route will default to a GET request
@app.route("/get_hero")
def get_hero():
    data = request.get_json()
    name = data["name"]
    name = "_".join(name.lower().split(" "))

    if name not in DATABASE.keys():
        message = {"message": "hero not found"}
        return make_response(message, 400)

    # so our response is going to a dictionary where the key is the name of the hero
    # and the value is the win and loss dictionary. It's the same as a single entry
    # of the database but we have to structure it manually back to the user in the
    # response because of how dictionaries are accessed i.e.  if our DATABASE was a 
    # list of list we could have returned the list, but it's not. If that was confusing
    # just ignore what I said.
    return jsonify({
        name: DATABASE[name]
    })

# so now we want to get a list of all the heroes and their win loss record
# this is actually quite easy because our DATABASE is actually a dictionary.
# in future projects you'll have to construct a response, but in this case you 
# can just return the jsonify'd version of the database
@app.route("/get_heroes")
def get_heroes():
    return jsonify(DATABASE)


# SICK! So now you've created a basic API - this next section is a list of additional bonus activities
# that expand the functionality of this API.  This will be a good exercise on Googling how python works,
# or a nice revisit into the content that you've already learned and now have the opportunity to use.
# These get progressively harder but if you get stuck for too long on a single bonus task, just skip it
# and we can talk about how you would go about doing that when I'm back.


# We now want to get a win rate of a singluar hero. To do this you want to 
# - Find a hero and check if it exists - if not we want to return an error
# - Get the hero's record and then divide the wins by the total wins
# - return the result in a json payload
# BONUS: if you divide a number to get an average, you can have repeating numbers i.e 46.777777777777777777 
# how can you limit how long the win rate percentage is to say 2 or 3 digits i.e 46.77? 
@app.route("/hero_win_rate")
def hero_win_rate():
    return None


# Now we want to get to total win rate of all heroes that you've played. It's going to be very similar to the 
# last step but we want to get the win rates for all heroes.
# HINT: DATABASE.values() will return a list of dictionaries of all win rates. 
# You could can set up a "win" and "loss" variable (outside the for loop) and a for loop to get the values 
# and use +- or += to update the counts
@app.route("/win_rate")
def win_rate():
    return None


# You've been working hard on writing this program, but it's getting pretty annoying losing all your data
# when you're writing all this shit.  Let's make an endpoint called save which will write database to 
# a database.json file.  
# You want to:
# - open a file (google "with open" on how to use a context manager)
# - turn/dump the dictionary into json
# - write the json
# - rewrite the beginning of this API where DATABASE is introduced to check if there is a database.json file, 
#   if so, open it and set DATABASE as that rather than an empty dictionary
@app.route("/save")
def save():
    return None


# You're pulling a me and starting to notice that all you want to do is play Axe. That's no way to live is it?
# You decide that you want to play every single hero and want to get a list of all the heroes that you 
# have not played. To do this:
# - find a list of all the heroes on the internet and convert them into our hero format ("faceless_void")
# - have that list of heroes as a list variable
# - get a list of all the heroes that you have played (remember DATABASE.keys() will be that)
# - find the differences between the two lists, and return the final list as the result
# BONUS: you'll probably start this task with a for loop (which is totally reasonable) but python has a thing
# called "list comprehensions".  Google it and see if you can make this process into one line.

@app.route("/unplayed_heroes")
def unplayed_heroes():
    return None


# NOTE: FEEL FREE TO SKIP THIS ONE IT'S PRETTY HARD, even I'd be annoyed to do this and would have to look it up
# You've played a lot of Dota and recorded a lot of stats. Now you want to see a list
# what heroes you've won the most with (and we do not care about losses). While Python
# now has all dictionaries as ordered dictionaries, Sorting a dictionary of dictionaries is quite advanced.
# What you want to do is:
# - create a list of dictionaries where the key is the hero name, and the value is the wins i.e. [{"faceless_void": 5}, {"axe": 10}]
# - sort that list using sorted and a lambda (i hate lambdas but it is the easiest way to do it)
# - basically just do what they do in this stackoverflow post: https://stackoverflow.com/questions/72899/how-do-i-sort-a-list-of-dictionaries-by-a-value-of-the-dictionary
#   and try and kind of understand what is going on
# - return the list
@app.route("/most_winning_heroes")
def most_winning_heroes():
    return None

"""
Hopefully you found the guide and the exercises pretty fun, there's a few directions that you can go 
from this now that you've experienced some coding.  Here's some things to google based on what you're 
interested in:

# Do you want to make an actual website to do all this shit instead of using Postman?

- Flask is my choice to make APIs but it can also serve HTML websites.  Most people don't really
  do websites like this anymore but it is good to know.  Look at Flask templates: 
  https://www.digitalocean.com/community/tutorials/how-to-use-templates-in-a-flask-application

- All the cool kids use javascript to make websites now. The idea is that the javascript is the client
  makes API calls and then renders it.  This is how most website development works now. People mostly
  use "frameworks" to develop in and Svelte is kind of the new hotness (you may have heard of things like
  react, vue, angular, etc. those are all frameworks) check out the tutorial.  it's in javascript
  but honestly, one day you will learn that it's all basically the same fuckin shit:
  https://svelte.dev/tutorial/basics

# Do you want to make this current API more "real" by using a database? 
- SQL is the a relational database language that you'll pretty much be using forever,
  so take the time to learn how that works. Databases are essentially giant ass excel
  tables that are persistent. Check out: https://www.sqltutorial.org/

- You can try and create a database in SQLite and attach it to your app so you can have it 
  actually save the data whenever you make a call, and not just when you hit the /save endpoint
  https://www.digitalocean.com/community/tutorials/how-to-use-the-sqlite3-module-in-python-3

- You can also look into how ORMs like SQLalchemy and PeeWee, but that's probably way too advanced for right now.

# Do you want to interact with other data?
- right now the only data that you currently have is what you've entered into your database manually.
  The world kind of changes when you can use other people's API to do stuff.  In order to do that
  you need to learn how to make HTTP requests in order to process other people's JSON.  The
  number 1 http library in my opinion is requests: https://requests.readthedocs.io/en/latest/

- after you learn how to make an HTTP request, check out free apis that you can get data from and
  try fucking around with the data.  For example, https://pokeapi.co/ is a free API about pokemon.
  Try making some requests there and playing around with the JSON responses from that

# Do you want to save your work somewhere else so you can get your code from any computer
  and also save "versions" of your code so if you break something you can undo it?
- Look into how Git works: https://www.freecodecamp.org/news/git-and-github-for-beginners/
  I can 100% promise you that if you ever want to get a job or get a job in development
  you'll HAVE to know the basics of this so I would recommend looking into it

# Do other things with Python libaries
- Python is dope because it has probably the most libraries of any programming language meaning
  there's a lot of premade code that you can just run and do really amazing and fun shit.

  Flask and Requests are 2 of the most popular but i would encourage you to fuck around with 
  some other libraries just to see what is possible.

  - QT is a libary that can let you make desktop apps. Try their hello world tutorial: 
    https://github.com/pyqt/examples/tree/_/src/01%20PyQt%20QLabel

  - Pandas is some data science shit where you can work on data and make graphs and charts. Try their tutorial with excel files
    https://www.dataquest.io/blog/excel-and-pandas/

  


Future topics to google but probably outside of your ability level for now but definitely google it:
- Authentication i.e. usernames and passwords
- Deploying code to a server on the internet (i.e. getting a Digital Ocean server)
- Containerization (i.e Docker, Kubernetes)
- 
"""


if __name__ == "__main__":
    app.run(debug=True)
