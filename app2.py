from flask import Flask, request, jsonify

app = Flask(__name__)

DATABASE = {}

"""
DATABASE = {
  {
    "faceless_void": {"win": 5, "loss": 0},
    "axe": {"win": 100, "loss": 9}
  }
}
"""

@app.route("/")
def hello_world():
    return "hello world"

@app.route("/add_hero", methods=["POST"])
def add_hero():
    data = request.get_json()
    # data = {'name': 'Faceless Void'}
    name = data["name"] # name = Faceless Void
    name = name.lower() # name = "faceless void"
    
    # version a
    name = name.replace(" ", "_")

    # version b
    # name = name.split(" ") # name = ["faceless", "void"]
    # name = "_".join(name) # name = "faceless_void"

    if name in DATABASE.keys():
        return jsonify({"message": "hero already added"})

    DATABASE[name] = {"win": 0, "loss": 0}
    
    print("THIS THE DATABASE", DATABASE)

    return jsonify({"message": "hero added!"})

# lets add some fuckin heroes baby
@app.route("/update_hero", methods=["POST"])
def update_heroo():
    # our client is gonna send code that looks like
    # {"name": "Faceless Void", "result": "win"} 
    # {"name": "Axe", "result": "loss"}

    data = request.get_json()
    # data = {'name': 'Faceless Void'}
    name = data["name"] # name = Faceless Void
    name = name.lower() # name = "faceless void"
    
    # version a
    name = name.replace(" ", "_") # name = "faceless_void"

    result = data.get("result", "")
    # there are 2 outcomes, wins or a loss, and we don't get that, we return a helpful error

    if result == "win":
        # DATABASE[name] # the value is a dictionary {"win": 0, "loss": 0}
        DATABASE[name]["win"] += 1
        print("THIS THE DATABASE", DATABASE)

        return jsonify({"message": "win has been added"})
    elif result == "loss":
        DATABASE[name]["loss"] += 1
        print("THIS THE DATABASE", DATABASE)
        return jsonify({"message": "loss has been added"})
    else:
        return jsonify({"message": "result is missing and must be a win or a loss"})
    


if __name__ == "__main__":
    app.run(debug=True)